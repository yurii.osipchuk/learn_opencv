# Install OpenCV:

## Build with opencv_contrib:

### Install minimal prerequisites (Ubuntu 18.04 as reference)
- `sudo apt update && sudo apt upgrade -y`
- `sudo apt install -y cmake g++ wget unzip`
### Download and unpack sources
- `wget -O opencv.zip https://github.com/opencv/opencv/archive/master.zip`
- `wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/master.zip`
- `unzip opencv.zip`
- `unzip opencv_contrib.zip`
### Create build directory and switch into it
- `mkdir -p build && cd build`
### Configure
- `cmake -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib/modules ../opencv` - standard without ffmpeg
- `cmake -DWITH_FFMPEG=ON -DCMAKE_BUILD_TYPE=RELEASE -DCMAKE_INSTALL_PREFIX=/usr/local -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib/modules ../opencv`
- default dynamic library
- static library use: `-DBUILD_SHARED_LIBS=OFF`
### Build
- `cmake --build .`
- `sudo make install`
### Other
By default OpenCV will be installed to the /usr/local directory, all files will be copied to following locations:
- `/usr/local/bin` - executable files
- `/usr/local/lib` - libraries (.so)
- `/usr/local/cmake/opencv4` - cmake package
- `/usr/local/include/opencv4` - headers
- `/usr/local/share/opencv4` - other files (e.g. trained cascades in XML format)
