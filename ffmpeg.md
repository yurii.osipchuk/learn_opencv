# FFMPEG:

### site: 
- [link:](https://www.ffmpeg.org/ffmpeg.html)
---

### Options:
- `-i` - input file
- `-f` - force format (формат конечного файла)
- `-vn` - disable video recording(конечный файл будет без видео)
- `-ab` - bitrate, set the audio bitrate in bit/s(default=64k, 192000 = 192 kbps)
- `-ar` - freq, set the audio sampling frequency, default=44100Hz (частота дискретизации аудио в Hz, широко используются значения 22050, 44100, 48000 Гц)
- `-ac` - channels, set the number of audio channels, default=1 (количество каналов аудио)
- `-hide_banner` - скрыть информацию о копирайте
- `-vf` - установить видео фильтр помогающий изменить скорость
- `-formats` - список поддерживаемых форматов

### Example:
- `ffmpeg -i "inFile.mp4" -f mp3 -ab 192000 -vn outFile.mp3` - извлечь аудио из видео
- `ffmpeg -i Movie.mkv -c copy -map 0:s:0 subs.01.srt -c copy -map 0:s:1 subs.02.srt` - извлечь субтитры из видео
    + `0:s:0` - text one
    + `0:s:1` - text two
- `ffmpeg -i input_video.mp4 -vcodec copy -acodec copy -ss 00:00:00 -to 00:00:00 trim_video.mp4` - вырезает фрагмент видео
    + `hh:mm:ss`
    + `-ss` - начальное время
    + `-to`  - конечное время
    + `-t` - какой интервал времени от начального(-t 00:01:00 = 1 min)

